﻿using BelajarRocketversity.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BelajarRocketversity.Infrastucture
{
    public class NoJoeOnMondaysAttribute : ValidationAttribute 
    {
        public NoJoeOnMondaysAttribute() {
            ErrorMessage = "Joe cannot book apppointments on Mondays";
        }

        public override bool IsValid(object value)
        {
            Appointment app = value as Appointment;
            if (app == null || string.IsNullOrEmpty(app.ClientName) || app.Date == null)
            {
                return true;
            } else
            {
                return !(app.ClientName == "Joe" && app.Date.DayOfWeek == DayOfWeek.Monday);
            }
        }
    }
}
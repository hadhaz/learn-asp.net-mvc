﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace BelajarRocketversity.Controllers
{
    public class ExampleController : Controller
    {
        // GET: Example
/*        public ActionResult Index()
        {
            return View("Homepage");
        }*/

/*        public ViewResult Index()
        {
            return View();
        }*/

/*        public ViewResult Index()
        {
            return View("~/Views/Other/Index.cshtml");
        }
*/   
       /* public ViewResult Index()
        {
            DateTime date = DateTime.Now;
            return View(date);
        } */

        public ViewResult Index()
        {
            ViewBag.Message = "Hello";
            ViewBag.Date  = DateTime.Now;
            return View();
        }

        /* public RedirectResult Redirect()
        {
            return RedirectPermanent("/Example/Index");
        }*/

        public RedirectToRouteResult Redirect()
        {
            TempData["Message"] = "Hello";
            TempData["date"] = DateTime.Now;

            return RedirectToRoute(new
            {
                controller = "Example",
                action = "Index",
                ID = "MyID"
            });
        }

        public HttpStatusCodeResult StatusCode()
        {
            return new HttpUnauthorizedResult();
        }
    }
}
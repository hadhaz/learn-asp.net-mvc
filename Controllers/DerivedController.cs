﻿using BelajarRocketversity.Infrastucture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace BelajarRocketversity.Controllers
{
    public class DerivedController : Controller
    {
        // GET: Derived
        public ActionResult Index()
        {
            ViewBag.Message = "Hello from the DerivedController Index method";
            return View("MyView");
        }

        public ActionResult ProduceOutput()
        {
            if (Server.MachineName == "TINY")
            {
                return new CustomRedirectResult { Url = "/Basic/Index"};
            } else
            {
                Response.Write("Controller: Derived, Action: Produce Output");
                return null;
            }
        }

    }
}
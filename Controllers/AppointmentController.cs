﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BelajarRocketversity.Models;
using Microsoft.SqlServer.Server;

namespace BelajarRocketversity.Controllers
{
    public class AppointmentController : Controller
    {
        // GET: Appointment
        public ActionResult Index()
        {
            return View();
        }

        public ViewResult MakeBooking()
        {
            return View(new Appointment { Date = DateTime.Now});
        }

        [HttpPost]
        public ViewResult MakeBooking(Appointment appointment)
        {

            if (ModelState.IsValid)
            {
                return View("Completed", appointment);
            }

            return View();
            
        }
    }
}